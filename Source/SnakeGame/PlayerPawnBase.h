// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
		ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeBase> SnakeActorClass;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable,Category = "SnakePawn")
	void CreateSnakeActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

	float MinY = -1500.f;
	float MaxY = 1500.f;
	float MinX = -800.f;
	float MaxX = 800.f;
	float SpawnZ = 50.f;

	void AddRandomApple();
	 
	float StepDelay = 1.0f;
	
	float BuferTime = 0;

	int32 GameMode = 0;

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
		int32 GetGameMode() const { return GameMode; }

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
		int32 GetScore();
	
	bool GamePause = false;

	UFUNCTION(BlueprintCallable, Category = "SnakePawn")
		bool GetGamePause() const { return GamePause; }

	void FMove(float ButtonVal);
};
